#! /usr/bin/env python3

import sys
import os

import re

def main():
    if len(sys.argv) < 2:
        print("USAGE: %s <file name>" % sys.argv[0])
        return 2

    f = open(sys.argv[1], 'r')

    lines = []

    for line in f:
        lines.append(line.split('\n')[0])

    f.close()

    titles = []

    for line in lines:
        if re.match(r'^##### ', line):
            titles.append(re.sub(r'^##### ', r'', line).split('\n')[0])

    print('#### List\n')

    titles.sort()

    for title in titles:
        link = re.sub(r'\.', r'', title)
        t = "* [%s](#%s)" % (title, link)
        print(t)

    skip_lines = True

    for line in lines:
        if skip_lines and line == '#### Descriptions':
            skip_lines = False
            print()

        if not skip_lines:
            print(line)


if __name__ == '__main__':
    sys.exit(main())
