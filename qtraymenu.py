#! /usr/bin/env python3

'''
=====================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=====================================================================

QTrayMenu - custom menu with pipe support. Based on PyQt4.

Author:     batekman@gmail.com
Website:    https://github.com/batekman/scripts

Source file example:

konsole # Launchs konsole, label is "konsole"
xterm " Slowest terminal emulator # Launchs xterm, label is after quote ;)
konqueror " # Label is same as command, but with uppercase first letter
* Games # Menu
    * roguelike # Submenu
        nethack
    * action
        * open-source # Subsubmenu
            openarena
        * proprietary
            quake3
    minetest # Item in submenu
* Documents
    @ls ~/Documents # Pipe menu
systemsettings " System Settings
- # separator
%Quit # Exits program, label is "Quit"

Note that "ls" not generates correct menu. Please use special scripts.

=====================================================================

'''

PNAME="QTrayMenu"

import sys,time,os,re
from PyQt4 import QtGui,QtCore

def readMenuFile():
    f=open("menu.txt","r")
    s=[]
    for line in f:
        s.append(line)
    f.close()
    return(s)

def launchItem(s):
    sys.stderr.write("Starting \""+s+"\"\n")
    os.system(s)

def addItem(menu,s):
    s=s.split("\n")[0]
    if(re.match("^-$",s)):
        menu.addSeparator()
        return()
    if(re.match("^%",s)):
        menu.addAction(s[1:]).triggered.connect(lambda: sys.exit())
        return()
    if(re.match("^@",s)):
        t=os.popen(s[1:])
        parseSrc(t,menu)
        return()
    if(re.search(" \" ",s)):
        t=s.split(" \" ")
        cmd=t[0].strip(" ")
        title=t[1].strip(" ")
    elif(re.search(" \"$",s)):
        t=s[:len(s)-2]
        title=t[0].upper()+t[1:]
        cmd=t
    else:
        title=s
        cmd=s
    menu.addAction(title).triggered.connect(lambda: launchItem(cmd))

def checkComments(s):
    if(not "#" in s):
        return(s)
    elif(re.match("^#",s)):
        return("")
    else:
        s=s.split("#")[0]
        return(s)

def parseSrc(source,menu):

    l=0
    m={}
    m[0]=menu

    for line in source:
        line=checkComments(line).strip(" ")
        if(line==""):
            continue
        while(not re.match("^\t{"+str(l)+"}",line)):
            l=l-1
        line=line[l:]
        if(re.match("^\* ",line)):
            l=l+1
            m[l]=m[l-1].addMenu(line[2:])
        else:
            addItem(m[l],line)

def refreshMenu(menu):
    menu.clear()
    source=readMenuFile()
    parseSrc(source,menu)

def showMenu(menu,pos):
    menu.move(pos)
    menu.show()

def main():

    app = QtGui.QApplication(sys.argv)
    menu = QtGui.QMenu()

    refreshMenu(menu)

    if(len(sys.argv)==2) and (sys.argv[1]=="--show"):
        # Show-once mode
        showMenu(menu,QtGui.QCursor.pos())
        menu.aboutToHide.connect(lambda: app.quit())
    else:
        # Default tray mode
        tr=QtGui.QSystemTrayIcon(QtGui.QIcon("/usr/share/pixmaps/xterm_32x32.xpm"), app)
        tr.setContextMenu(menu)
        tr.setToolTip(PNAME)
        tr.activated.connect(lambda: showMenu(menu,QtGui.QCursor.pos()))
        tr.show()
        # Auto-refresh
        fwatcher=QtCore.QFileSystemWatcher(["menu.txt"])
        fwatcher.connect(fwatcher, QtCore.SIGNAL('fileChanged(QString)'), lambda: refreshMenu(menu))

    #####
    sys.exit(app.exec_())


if __name__=="__main__":
    main()
