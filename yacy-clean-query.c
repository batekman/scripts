#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>

#define LINE_LENGTH 40

int generateQuery(FILE *file)
{
    char str[LINE_LENGTH];
    char *s;
    int i = 0;
    printf("-host_s:(");
    while(fgets(str, LINE_LENGTH, file) != NULL)
    {
        if(i>0) printf("|");
        s = strtok(str, "\n");
        printf("\"%s\"", s);
        i++;
    }
    printf(")\n");

    return 1;
}

int main(int argc, char *argv[])
{
    FILE *f;
    char* fname = "";
    int opt;

    while((opt = getopt(argc, argv, "f:")) != -1)
    {
        switch(opt)
        {
            case 'f':
                fname = optarg;
                break;
            default:
                return 2;
        }
    }

    if(argc<2) return 2;

    f = fopen(fname, "r");
    if(f==NULL) return 1;
    generateQuery(f);
    fclose(f);

    return 0;
}
