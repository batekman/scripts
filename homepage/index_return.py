#! /usr/bin/env python3

'''
=====================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=====================================================================

'''

import sys
import os
import traceback
import re
import datetime
import urllib.parse
import urllib.request
import shutil
import time
import operator
from lxml import etree as ET

import counter

iconlist = []


def downloadAllIcons():
    for a in iconlist:
        url = a[0]
        localfile = a[1]
        try:
            with urllib.request.urlopen(url, timeout=10) as response, open(localfile, 'wb') as out_file:
                shutil.copyfileobj(response, out_file)
                out_file.close()
        except:
            if not os.path.isfile(localfile):
                fhandle = open(localfile, "wb")
                fhandle.close()


def getIcon(url, localfile):
    localfile = localfile.encode('utf-8')
    fromts = datetime.datetime.fromtimestamp
    mtime = os.path.getmtime
    if os.path.isfile(localfile) and (datetime.datetime.now() - fromts(mtime(localfile))).days < 30:
        return True
    else:
        iconlist.append((url, localfile))
        return False

def createDiv(parent="", className="", idName=""):
    if parent:
        result = ET.SubElement(parent, "div")
    else:
        result = ET.Element("div")

    if className:
        result.set("class", className)

    if idName:
        result.set("id", idName)

    return result


def main():
    cfg = {}
    cfg_file = open("config", "r", encoding="utf-8")
    for line in cfg_file:
        if "=" in line:
            param = line.split("=")[0].strip()
            value = line.split("=")[1].strip()
            cfg[param] = value
    cfg_file.close()

    try:
        html = {}
        html["html"] = ET.Element("html")
        html["head"] = ET.SubElement(html["html"], "head")
        html["body"] = ET.SubElement(html["html"], "body")

        html["meta"] = ET.SubElement(html["head"], "meta")
        html["meta"].set("http-equiv", "Content-Type")
        html["meta"].set("content", "text/html; charset=utf-8")

        html["title"] = ET.SubElement(html["head"], "title")
        html["title"].text = "New tab"

        tt = int(time.mktime(time.localtime()))

        html["CSS"] = ET.SubElement(html["head"], "link")
        html["CSS"].set("href", "style.css?%d" % tt)
        html["CSS"].set("type", "text/css")
        html["CSS"].set("rel", "stylesheet")

        html["script"] = ET.SubElement(html["head"], "script")
        html["script"].set("language", "JavaScript")
        html["script"].set("type", "text/javascript")
        html["script"].set("src", "handler.js?%d" % tt)
        html["script"].text = " "

        html["outer-table"] = ET.SubElement(html["body"], "div")
        html["outer-table"].set("class", "outer-table")

        html["section search"] = ET.SubElement(html["outer-table"], "div")
        html["section search"].set("class", "section search")

        html["section search -> title"] = ET.SubElement(html["section search"], "div")
        html["section search -> title"].set("class", "section-title")
        html["section search -> title"].text = "Поиск"

        f = open("search-engines.txt", "r", encoding="utf-8")

        i = 0
        html["search item"] = {}

        searchs={}

        for line in f:
            name = line.split(" \" ")[0]
            nameid = "Search-" + name
            addr = line.split(" \" ")[1].split("\n")[0]
            prefix = addr.split("/")[0]

            name_enc = urllib.parse.quote_plus(name)
            icourl = prefix + "//" + addr.split("/")[2] + "/favicon.ico"
            icoaddrl = os.path.join('search_icons', name_enc + '.ico')
            if getIcon(icourl, icoaddrl):
                icoaddr = "%s/search_icons/%s.ico?%d" % (cfg["docroot"], name_enc, tt)
            else:
                icoaddr = icourl

            mainpage = prefix + "//" + addr.split("/")[2] + "/"

            searchs[addr] = {}

            searchs[addr]['icoaddr'] = icoaddr
            searchs[addr]['mainpage'] = mainpage
            searchs[addr]['addr'] = addr
            searchs[addr]['name'] = name
            searchs[addr]['nameid'] = nameid
            searchs[addr]['i'] = i
            searchs[addr]['count'] = counter.getCount(addr)

            i = i + 1

        f.close()

        i = 0

        searchs_sorted = sorted(searchs.items(), key=lambda x: x[1]['i'])

        i = 0
        for search_item in sorted(searchs_sorted, key=lambda x: x[1]['count'], reverse=True):
            search = search_item[1]
            html["search item"][i] = ET.SubElement(html["section search"], "div")

            item = html["search item"][i]
            item.set("class", "search item")

            link = ET.SubElement(item, "a")
            link.set("style", "text-decoration:inherit;color:inherit;")
            link.set("title", search['mainpage'])
            link.set("href", search['mainpage'])

            title = ET.SubElement(link, "div")
            title.set("class", "search item title")
            title.text = search['name']

            icon = ET.SubElement(link, "img")
            icon.set("style", "float: left;margin-top: 5px;margin-right:5px;padding:0;visibility:hidden;")
            icon.set("class", "favicon")
            icon.set("onload", "this.style.visibility='inherit';")
            icon.set("src", search['icoaddr'])

            field = ET.SubElement(item, "div")
            field.set("class", "search item field")

            fieldInput = ET.SubElement(field, "input")
            fieldInput.set("type", "text")
            fieldInput.set("id", search['nameid'])
            fieldInput.set("class", "search item field")
            fieldInput.set("onkeydown", "javascript:if(event.keyCode == 13) Search(\"%s\", \"%s\");" % (search['addr'], search['name']))

            if i == 0:
                fieldInput.set("autofocus", "true")

            button = ET.SubElement(item, "div")
            button.set("class", "search item button")

            buttonInput = ET.SubElement(button, "input")
            buttonInput.set("class", "search item button")
            buttonInput.set("id", search['nameid'] + "-btn")
            buttonInput.set("type", "submit")
            buttonInput.set("value", "искать")

            # "Search" is function which redirects
            # browser to search page
            # and adds one point to it's counter
            buttonInput.set("onclick", "Search(\"%s\", \"%s\");" % (search['addr'], search['name']) )


            i = i + 1


        html["section bookmarks"] = ET.SubElement(html["outer-table"], "div")
        html["section bookmarks"].set("class", "section bookmarks")

        html["section bookmarks -> title"] = ET.SubElement(html["section bookmarks"], "div")
        html["section bookmarks -> title"].set("class", "section-title")
        html["section bookmarks -> title"].text = "Закладки"

        f = open("bookmarks.txt", "r", encoding="utf-8")

        i = 0

        html["bookmarks item"] = {}
        bookmarks = {}

        for line in f:
            t = line.split("\n")[0]
            if " \" " in t:
                addr = t.split(" \" ")[0]
                title = t.split(" \" ")[1]
            else:
                addr = t
                title = t
            if not re.match("^https?://", addr):
                fulladdr = "http://" + addr
            else:
                fulladdr = addr
            if "://" in title:
                title = title.split("://")[1]

            bookmarks[fulladdr] = {}

            bookmarks[fulladdr]['count'] = counter.getCount(fulladdr)
            bookmarks[fulladdr]['addr'] = addr
            bookmarks[fulladdr]['title'] = title

            icourl = prefix + "//" + fulladdr.split("/")[2] + "/favicon.ico"
            title_enc = urllib.parse.quote_plus(title)
            icoaddrl = os.path.join("bookmarks_icons", title_enc + ".ico")
            if getIcon(icourl, icoaddrl):
                icoaddr = "%s/bookmarks_icons/%s.ico?%d" % (cfg["docroot"], title_enc, tt)
            else:
                icoaddr = icourl

            bookmarks[fulladdr]['icoaddr'] = icoaddr
            bookmarks[fulladdr]['fulladdr'] = fulladdr
            bookmarks[fulladdr]['i'] = i
            i = i + 1
        f.close()

        bookmarks_sorted = sorted(bookmarks.items(), key=lambda x: x[1]['i'])

        i = 0
        for bookmark_item in sorted(bookmarks_sorted, key=lambda x: x[1]['count'], reverse=True):
            bookmark = bookmark_item[1]
            html["bookmarks item"][i] = ET.SubElement(html["section bookmarks"], "div")
            html["bookmarks item"][i].set("class", "bookmarks item")

            link = ET.SubElement(html["bookmarks item"][i], "a")
            link.set("title", bookmark['title'])
            link.set("href", bookmark['fulladdr'])

            linkdiv = ET.SubElement(link, "div")
            linkdiv.set("class", "bookmarks item link")

            # Count bookmark's usage frequency
            linkdiv.set("onclick", "Counter(\"Bookmarks\", \"%s\", \"%s\");" % (bookmark['fulladdr'], bookmark['title']))

            icon = ET.SubElement(linkdiv, "img")
            icon.set("class", "bookmarks item favicon")
            icon.set("onerror", "this.style.visibility='hidden';")
            icon.set("src", bookmark['icoaddr'])

            icon.tail = bookmark['title']

            i = i + 1

        t = ET.fromstring(ET.tostring(html["html"], encoding="UTF-8"))

        return ET.tostring(t,
            encoding="UTF-8", xml_declaration=False,
            pretty_print=True, doctype="<!DOCTYPE html>").decode("utf-8")

    except:
        traceback.print_exc(file=sys.stdout)
        pass

if __name__ == "__main__":
    main()
