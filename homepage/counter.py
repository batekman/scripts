#! /usr/bin/env python3

'''
=====================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=====================================================================

'''

import sys
import os
import sqlite3

def count(typ, addr, name):
    conn = sqlite3.connect('counter_db.sqlite')
    c = conn.cursor()

    c.execute('''
    CREATE VIRTUAL TABLE IF NOT EXISTS
    counts USING fts4(
    type text,
    addr text,
    name text,
    count int);
    ''')

    conn.commit()

    count = getCount(addr, conn, False)

    if count == 0:
        c.execute("INSERT INTO counts (type, addr, name, count) VALUES (?, ?, ?, ?)", (typ, addr, name, count + 1))
    else:
        c.execute("UPDATE counts SET count = ? WHERE addr = ?", (count + 1, addr))

    conn.commit()

    conn.close()

def getCount(addr, conn=sqlite3.connect('counter_db.sqlite'), close=True):
    c = conn.cursor()
    ans = c.execute("SELECT name FROM sqlite_master WHERE type = 'table' AND name = 'counts'")

    if ans.fetchone() is None:
        result = False
    else:
        ans = c.execute("SELECT * FROM counts WHERE addr MATCH ?;", (addr,)).fetchone()
        if ans is None:
            count = 0
        else:
            count = ans[3]
        result = count

    #if close:
    #    conn.close()

    return result
