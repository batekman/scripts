#!/usr/bin/env python3

'''
=====================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=====================================================================

'''

import sys, os
import bottle
import index_return
import counter
import re
import threading


@bottle.route('/_counter', 'POST')

def count():
    typ = bottle.request.POST.get('type')
    name = bottle.request.POST.get('name')
    addr = bottle.request.POST.get('addr')
    counter.count(typ, addr, name)


@bottle.route('/_load_icons')

def load_icons():
    return index_return.downloadAllIcons()

@bottle.route('/style.css')

def send_stylesheet():
    if os.path.isfile("style.css"):
        return bottle.static_file("style.css", root='', mimetype='text/css')
    else:
        return False


@bottle.route('/handler.js')

def send_js():
    if os.path.isfile("handler.js"):
        return bottle.static_file("handler.js", root='', mimetype='text/javascript')
    else:
        return False


@bottle.route('/<dir>/<filename:re:.*\.ico>')

def send_ico(dir, filename):
    if os.path.isfile("%s/%s" % (dir, filename)):
        return bottle.static_file(filename, root=dir, mimetype='image/ico')
    else:
        return False


@bottle.route('/')

def index():
    thread = threading.Thread(target=index_return.downloadAllIcons, args=())
    thread.daemon = True
    thread.start()
    return index_return.main()


bottle.run(host='localhost', port=8080)
