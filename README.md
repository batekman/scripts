#### List

* [Monokai-soft.colorscheme](#Monokai-softcolorscheme)
* [addpony](#addpony)
* [barcode](#barcode)
* [bashcomp-update](#bashcomp-update)
* [bindec.py](#bindecpy)
* [dcutf.c](#dcutfc)
* [distdrop](#distdrop)
* [e](#e)
* [e-time](#e-time)
* [ebuild-autoformat.py](#ebuild-autoformatpy)
* [fansilence.c](#fansilencec)
* [get-ath3k-firmware](#get-ath3k-firmware)
* [gitc](#gitc)
* [gpo-biggest](#gpo-biggest)
* [hex2rgb](#hex2rgb)
* [infoscreen](#infoscreen)
* [ktorrent-shy](#ktorrent-shy)
* [linux-3.15pf-np300e5c-u08.config](#linux-315pf-np300e5c-u08config)
* [linux-4.2.5gentoo-np300e5c-u08.config](#linux-425gentoo-np300e5c-u08config)
* [live_updater](#live_updater)
* [lorponylist.txt](#lorponylisttxt)
* [loruser.go](#lorusergo)
* [ol-cleaner](#ol-cleaner)
* [play_link](#play_link)
* [qtraymenu.py](#qtraymenupy)
* [quse](#quse)
* [readme_toc.py](#readme_tocpy)
* [rtc.c](#rtcc)
* [rtc.py](#rtcpy)
* [sbg](#sbg)
* [sched](#sched)
* [showsizes](#showsizes)
* [shy-mode](#shy-mode)
* [synconfig](#synconfig)
* [tasf.sh](#tasfsh)
* [upnp](#upnp)
* [upright](#upright)
* [virtualbox](#virtualbox)
* [walcleaner](#walcleaner)
* [wt](#wt)
* [xmpp-cmdbot.go](#xmpp-cmdbotgo)
* [yacy-clean-query.c](#yacy-clean-queryc)

#### Descriptions

##### addpony
*Depends: wget, imagemagick*

Allows me to press one hotkey to add new link to LORPonyList.

##### barcode
*Depends: zbar, curl*

Starts zbarcam and browser with description of scanned barcode.

##### bashcomp-update
*Depends: gentoo, bash-completion*

Parses your shell's history and turns on completion for needed commands.

##### bindec.py
*Depends: python3*

Small library for conversion between decimal and binary values, and boolean arrays.

##### dcutf.c

Replacement for "cut" and "fold" utilities with wide-character support.

##### distdrop
*Depends: portage, bash*

Tool for dropping files to portage's DISTDIR.

##### get-ath3k-firmware
*Depends: wget*

This script downloads firmware files for my laptop's Bluetooth adapter. My laptop is Samsung NP300E5C-U08.

##### e
*Depends: portage*

Wrapper for some portage internal commands and portage-related utilities.

##### e-time
*Depends: portage, read access to /var/log/emerge.log*

Lets you find out compile time of your packages. Simple and stupid genlop replacement.

##### ebuild-autoformat.py

Checks ebuild for "/usr/portage/header.txt" in head, and if it's not found, puts it.

##### fansilence.c
*Depends: samsung-laptop module*

Small daemon that stops your laptop's fan when CPU temperature is low.

##### gitc
*Depends: bash, git*

Simple and comfortable "git commit" wrapper for commiting a single file.

##### gpo-biggest

Show N biggest overlays from gpo.zugaina.org.

##### hex2rgb
*Depends: octave*

Stupid and slow script that converts hex RGB value from STDIN to decimal, comma separated.

##### infoscreen
*Depends: conky, feh, imagemagick*

Script for temporarily starting conky with simulated blur.

##### ktorrent-shy
*Depends: xprintidle*

Stops all torrents when user activity is detected.

##### live_updater
Finds VCS-specific dirs in subdirs of current directory and starts commands to update repos.

##### linux-3.15pf-np300e5c-u08.config
*Depends: linux 3.15 sources with PF patchset*

It's my kernel config for my Samsung NP300E5C-U08 laptop. It's not clean, but all laptop's stuff works.

##### linux-4.2.5gentoo-np300e5c-u08.config
*Depends: linux 4.2.5*

Config for a newer kernel version.

##### loruser.go

Script for managing local LOR users database.

##### lorponylist.txt
*Depends: AdBlock*

List of filters for locking pony avatars on LINUX.ORG.RU.
Just use one of these addresses:
https://gitlab.com/batekman/scripts/raw/master/lorponylist.txt
https://raw.github.com/batekman/scripts/master/lorponylist.txt

##### Monokai-soft.colorscheme
Colorscheme for Konsole made from default Dark pastel theme's background and foreground, and Monokai theme for xterm found in the internets.

##### ol-cleaner
*Depends: eix*

Cleans useless overlays (use portconf instead of this shit).

##### play_link
*Depends: vlc, youtube-dl*

Plays address from X clipboard in vlc.

##### qtraymenu.py
*Depends: python3, PyQt4*

Custom menu with pipe support. Unfinished, but works. See the source file for help.

##### quse
*Depends: python3, PyQt4*

Tool for managing Gentoo USE-flags (currently READ-ONLY).

##### readme_toc.py
*Depends: python3*

Table of Contents generator for my README.md.

##### rtc.py
*Depends: python3*

Sets RTC wakeup time to given as argument in HH:MM format.

##### rtc.c
*Depends: C compiler*

Same as rtc.py but written on C language.

##### sbg
Just starts command in background and redirects its output to /dev/null.

##### sched
Outputs univeristy's agenda to conky format.

##### showsizes
Shows sizes of ALL files and subdirs in current directory.

##### synconfig
*Depends: synclient*

Setups config for synaptics touchpad driver from file.

##### shy-mode
*Depends: cpufrequtils, hdparm, root preveliges*

Turns ON/OFF powersaving mode for your CPU, HDD and both.

##### tasf.sh

True-minimalistic task manager. Shows last edited files at the end, meaning that their contents should receive attention in the first place (the LIFO principle).

##### upnp
*Depends: miniupnpc, upnpscan, iproute2, bash*

Simple upnpc wrapper.

##### upright
*Depends: python3*

Simple script that gets computer's uptime after its start and resume.

##### virtualbox
*Depends: sudo*

Wrapper script for VirtualBox correct start.

##### walcleaner
*Depends: python3*

Searches trash on your Walkman NWZ-E463 (and may be, some other models) and cleans it.

##### wt
*Depends: bash, sudo*

Creates a temporary directory, starts a shell in it and deletes this directory after exit.

##### xmpp-cmdbot.go

Simple jabber bot for remote control.

##### yacy-clean-query.c

Generates correct query for YaCy's "Administration -> Index Administration -> Index Deletion -> Delete by Solr Query" by whitelist from chosen file.

### Requisites for donations

**WMR:** R210609141440

**WMZ:** Z134616197459

**WMU:** U383550974742
