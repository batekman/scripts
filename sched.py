#! /usr/bin/env python3

import sys,os,time,re,datetime,calendar,locale

locale.setlocale(locale.LC_ALL, '')

##############################################################
######################## CONSTANTS ###########################

agFile=open(os.environ['HOME']+"/edu/agenda","r")

cWhite='${color e9e9e9}'
cGrey='${color grey}'
cDarkGrey='${color 707070}'
cBlue='${color #9999FF}'
cPink='${color #ddaa99}'
cOrange='${color orange}'
cGreen='${color green}'
cGreenDark='${color #00c000}'
cCyan='${color cyan}'
cGreenGrey='${color #90a090}'

global lessonTypes
lessonTypes={}
lessonTypes["Лаб"]="лабораторная"
lessonTypes["Л"]="лекция"
lessonTypes["Пр"]="практика"
lessonTypes["Вб"]="по выбору"
lessonTypes["Прпр"]=""

lessonTypeColors={}
lessonTypeColors["Лаб"]=cBlue
lessonTypeColors["Л"]=cGreenDark
lessonTypeColors["Пр"]=cOrange
lessonTypeColors["Вб"]=cGreenGrey
lessonTypeColors["Прпр"]=cCyan

timeState={}
timeState[2]="перемена 10"
timeState[3]="первое занятие"
timeState[4]="второе занятие"
timeState[5]="перемена 5"

timeTab="${goto 35}"
nameTab="${goto 85}"
pDepTab="${goto 235}"


parityDepStr={}
parityDepStr[-1]=""
parityDepStr[0]=pDepTab+cPink+"н/н"
parityDepStr[1]=pDepTab+cBlue+"ч/н"

##############################################################
######################### CLASSES ############################

class lesson:

    def __init__(self,lString,mto):
        self.string=lString
        self.time=mto
        self.names={}
        self.names[-1]=self.string.split("#")[1]
        self.parityDepended=0
        n=self.names
        self.name=n[-1]
        if n[-1].count("|"):
            self.parityDepended=1
            n[0]=n[-1].split("|")[0]
            n[1]=n[-1].split("|")[1]
            self.name=n[self.time.parity]
        ts=lString.split("#")[0].split(":")
        self.timeString=ts[0].zfill(2)+":"+ts[1].zfill(2)
        self.hour=ts[0]
        self.minutes=ts[1]
    
    def getParityDep(self):
        res=-1
        if self.parityDepended==1:
            a=self.names[self.time.parity]
            if a.count(":"):
                a=a.split(":")[1]
            if self.getName()==a:
                res=self.time.parity
        return(res)
    
    def getName(self):
        res=self.name
        #if self.parityDepended==1:
            #res=self.name[self.time.parity]
        if res.count(":"):
            res=res.split(":")[1]
        return(res)

    def getType(self):
        res=self.name
        if res.count(":"):
            res=res.split(":")[0]
        return(res)
    
    def getState(self):
        res=0
        if self.getName()!="":
            res=1
        return(res)

    def getTypeString(self):
        res=lessonTypes[self.getType()]
        return(res)

    def getTypeColor(self):
        return(lessonTypeColors[self.getType()])

##############################################################

class mytime:

    def __init__(self,Y=time.strftime("%Y"),m=time.strftime("%m"),d=time.strftime("%d"),H=time.strftime("%H"),M=time.strftime("%M"),S=time.strftime("%S")):
        Y=int(Y)
        m=int(m)
        d=int(d)
        H=int(H)
        M=int(M)
        S=int(S)
        self.date=datetime.date(Y,m,d)
        self.datetime=datetime.datetime(Y,m,d,H,M,S)
        self.absTime=time.mktime(self.datetime.timetuple())
        self.time=time.localtime(self.absTime)
        self.weekDayNumber=self.date.isoweekday()
        self.weekDayString=calendar.day_name[self.weekDayNumber-1]
        self.mday=self.date.day
        self.parity=0
        if self.date.isocalendar()[1] % 2:
     	    self.parity=1

    def seekDate(self,backward,count):
        Y=time.localtime(self.absTime).tm_year
        m=time.localtime(self.absTime).tm_mon
        if backward==0:
            d=time.localtime(self.absTime+86400*count).tm_mday
        if backward==1:
            d=time.localtime(self.absTime-86400*count).tm_mday
        a=str(Y) + "-" + str(m) + "-" + str(d)
        mydate=time.strptime(a, "%Y-%m-%d")
        return(mydate)

    def getNearestWday(self,weekDay,backward=0):
        res=0
        if backward==0:
            for d in range(0,7):
                wd=time.localtime(self.absTime+86400*d)
                if wd.tm_wday+1==weekDay:
                    res=wd
        if backward==1:
            for d in range(0,7):
                wd=time.localtime(self.absTime-86400*d)
                if wd.tm_wday+1==weekDay:
                    res=wd
        return(res)

##############################################################
    
class myday:
    def __init__(self,mto,lessons):
        self.time=mto
        self.lessons=lessons

##############################################################
######################### FUNCTIONS ##########################

def getTimeState(lTime,currentTime):
    res=0
    #print(currentTime.absTime,lTime.absTime)
    if currentTime.absTime>=lTime.absTime:
        if currentTime.absTime<=lTime.absTime+(45*2+5)*60:
            res=1
        if currentTime.absTime>lTime.absTime+(45*2+5)*60:
            res=2
        if currentTime.absTime<lTime.absTime+45*60:
            res=3
        if currentTime.absTime>=lTime.absTime+45*60:
            res=5
        if currentTime.absTime>=lTime.absTime+(45+5)*60:
            res=4

    return(res);

##############################################################

def showAgenda():
    days={}
    lessons={}
    
    curTime=mytime()
    
    i=0
    Day=0
    DayClr={}
    
    for line in agFile.readlines():
        line=line.split("\n")[0]
        if re.match("^[0-9]$",line):
            s=0
            days[Day]=int(line)
            Yesterday=0
            if days[Day]<curTime.weekDayNumber-1:
                Day=Day+1
                s=1
                continue
            if days[Day]==curTime.weekDayNumber-1:
                Yesterday=1
            if Day<0:
                Day=Day
                s=1
                continue
            if Day>=curTime.weekDayNumber+2:
                #if Day!=7:
                break
            nWday=curTime.getNearestWday(days[Day],Yesterday)
            lTime=mytime(Y=nWday.tm_year,m=nWday.tm_mon,d=nWday.tm_mday)
            if nWday.tm_yday==time.localtime(curTime.absTime).tm_yday:
                lTime=mytime(Y=nWday.tm_year,m=nWday.tm_mon,d=nWday.tm_mday,H=nWday.tm_hour,M=nWday.tm_min,S=nWday.tm_sec)
            DayClr[0]=cWhite
            DayClr[1]=cGrey
            sys.stdout.write("${font :size=8}\n${font}"+nameTab+DayClr[Yesterday])
            sys.stdout.write(calendar.day_name[int(line.split("\n")[0])-1])
            sys.stdout.write(cGrey+" ("+calendar.month_name[lTime.date.month])
            sys.stdout.write(","+str(lTime.date.day)+") "+cDarkGrey)
            #sys.stdout.write("$hr${font :size=6}\n\n${font}")
            sys.stdout.write("${font :size=6}\n\n${font}")
            i=0
            Day=Day+1
            continue
        if s==1:
            continue
        ot=lTime.time
        lessons[i]=lesson(line,lTime)
        lessons[i]=lesson(line,mytime(Y=ot.tm_year,m=ot.tm_mon,d=ot.tm_mday,H=lessons[i].hour,M=lessons[i].minutes))
        if lessons[i].getState()==1:
            timeClr=cWhite
            if curTime.absTime>=lessons[i].time.absTime:
                timeClr=cGrey
                if curTime.absTime<=lessons[i].time.absTime+(45*2+5)*60:
                    timeClr=cGreen
            sys.stdout.write(timeTab+timeClr+lessons[i].timeString+nameTab)
            sys.stdout.write(lessons[i].getTypeColor()+lessons[i].getName())
            sys.stdout.write(parityDepStr[lessons[i].getParityDep()])#+'${alignr}')
            #sys.stdout.write(cGrey+lessons[i].getTypeString()+"\n")
            sys.stdout.write("\n")
            if timeClr==cGreen:
                TimeCurrent=curTime.time.tm_hour*60+curTime.time.tm_min
                TimeStart=lessons[i].time.time.tm_hour*60+lessons[i].time.time.tm_min
                TimeEnd=lessons[i].time.time.tm_hour*60+lessons[i].time.time.tm_min+(45*2+5)
                TimeTotal=TimeEnd-TimeStart
                TimeElapsed=TimeEnd-TimeCurrent
                TimeLast=TimeTotal-TimeElapsed
                c=str(int(TimeLast*100/95))
                d=str(int(100-int(c)))
                sys.stdout.write(nameTab+cGrey+timeState[getTimeState(lessons[i].time,curTime)]+" ( "+str(TimeLast)+" / "+str(TimeTotal)+", "+str(TimeElapsed)+" )\n")
                sys.stdout.write(nameTab+cGrey+"${execbar echo "+c+"%}\n")
        i=i+1

##############################################################
########################### MAIN #############################

showAgenda()

agFile.close
