#! /usr/bin/env python3

import sys,time,datetime,locale

locale.setlocale(locale.LC_ALL, '')


class mytime:

    def __init__(self,Y=time.strftime("%Y"),m=time.strftime("%m"),d=time.strftime("%d"),H=time.strftime("%H"),M=time.strftime("%M"),S=time.strftime("%S")):
        Y=int(Y)
        m=int(m)
        d=int(d)
        H=int(H)
        M=int(M)
        S=int(S)
        self.date=datetime.date(Y,m,d)
        self.datetime=datetime.datetime(Y,m,d,H,M,S)
        self.absTime=time.mktime(self.datetime.timetuple())


H=sys.argv[1].split(":")[0];
M=sys.argv[1].split(":")[1];

ct=mytime()
at=mytime(H=H,M=M,S=0)

cta=ct.absTime
ata=at.absTime

tdif=time.mktime(time.localtime())-time.mktime(time.gmtime())

if len(sys.argv)==3 and sys.argv[2]=="-l":
    ata=ata+tdif
    cta=cta+tdif

if ata<cta:
    ata=ata+60*60*24

f=open("/sys/class/rtc/rtc0/wakealarm","w")
f.write("0\n")
f.close

f=open("/sys/class/rtc/rtc0/wakealarm","w")
f.write(str(ata)+"\n")
f.close

f=open("/proc/driver/rtc","r")
for line in f:
    sys.stdout.write(line)
f.close
