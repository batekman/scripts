/*
=====================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=====================================================================
Author:     batekman@gmail.com
Website:    https://github.com/batekman/scripts
=====================================================================
*/

#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <time.h>
#include <string.h>
#include "sched.h"

int true_wday(int wday)
{
    int o;

    o = wday;

#ifndef _WEEK_FROM_SUNDAY
    if(!o) { o = 7; }
#endif

    return o;
}

int getNearestWeekday(struct tm *t, int dayOfWeek)
{
    int b;
    int i;
    int o;
    int w;

    int currentTime;
    struct tm *tt;

    b = 0;
    w = true_wday(t->tm_wday);

    // Show next week at weekend
    if(w < 6)   { i = -w; }
    else        { i = 0; }

    o = 0;

    while(b != 1)
    {
        currentTime = mktime(t)+24*i*3600;
        tt = localtime(&currentTime);

        if(true_wday(tt->tm_wday) == dayOfWeek)
        {
            b=1;
            o=currentTime;
            return o;
        }

        ++i;
    }

    return 0;
}

double timeDiff(struct tm *t, struct tm *st)
{
    int currentTimeTT; // current time
    int secondTimeTT; // target time

    double diff; // time difference
    int out; // result

    currentTimeTT = mktime(t);
    secondTimeTT = mktime(st);
    diff = difftime(secondTimeTT, currentTimeTT);

    return diff;
}

int timeStatus(struct tm *t, struct tm *st, int dur)
{
    int currentTimeTT; // current time
    int secondTimeTT; // target time

    double diff; // time difference
    int out; // result

    currentTimeTT = mktime(t);
    secondTimeTT = mktime(st);
    diff = difftime(secondTimeTT, currentTimeTT);

    out = 0;

    if(diff > 0)
        { out = 2; }

    if((diff >= - dur * 60) && (diff < 0))
        { out = 1; }

    return out;
}
int getWeekParity(struct tm *t) {
    int weekNumber;
    char week[3];
    strftime(week, 3, "%W", t);
    weekNumber = atoi(week);
    if (weekNumber % 2 == 0)
    {
        return 1;
    } else {
        return 0;
    }
}

int main(int argc, char* argv[])
{
    setlocale(LC_ALL,"");

    int curTime;
    struct tm curTimeSt;
    struct tm *ct;

    char s[100];
    char* filePath;

    int prt;
    int mday;

    FILE *f;

    int iday;
    struct tm *day;
    
    int endOfLessons;

    const char* clr[11];
    const char* clrT[3];
    const char* clrD[3];
    const char* lType[5];
    const char* dOW[7];
    const char* monS[12];

    int lTC[5];

    clr[0]="${color white}";
    clr[1]="${color e9e9e9}";
    clr[2]="${color grey}";
    clr[3]="${color 707070}";
    clr[4]="${color #9999FF}";
    clr[5]="${color #ddaa99}";
    clr[6]="${color orange}";
    clr[7]="${color green}";
    clr[8]="${color #00c000}";
    clr[9]="${color cyan}";
    clr[10]="${color #90a090}";

    clrT[0]=clr[2];
    clrT[1]=clr[7];
    clrT[2]=clr[0];

    clrD[0]=clr[2];
    clrD[1]=clr[0];
    clrD[2]=clr[0];

    lType[0]="Лаб";
    lType[1]="Л";
    lType[2]="Пр";
    lType[3]="Вб";
    lType[4]="Прпр";

    lTC[0]=4;
    lTC[1]=8;
    lTC[2]=6;
    lTC[3]=10;
    lTC[4]=9;

    // Days of week
    dOW[0]="Понедельник";
    dOW[1]="Вторник";
    dOW[2]="Среда";
    dOW[3]="Четверг";
    dOW[4]="Пятница";
    dOW[5]="Суббота";
    dOW[6]="Воскресенье";

    // Months
    monS[0]="Январь";
    monS[1]="Февраль";
    monS[2]="Март";
    monS[3]="Апрель";
    monS[4]="Май";
    monS[5]="Июнь";
    monS[6]="Июль";
    monS[7]="Август";
    monS[8]="Сентябрь";
    monS[9]="Октябрь";
    monS[10]="Ноябрь";
    monS[11]="Декабрь";

    curTime = time(NULL);
    ct = &curTimeSt;
    memcpy(ct, localtime(&curTime), sizeof(struct tm));

#ifdef AY
    ct->tm_mday = AY;
#endif
#ifdef _HOUR
    ct->tm_hour = _HOUR;
#endif
#ifdef _MIN
    ct->tm_min = _MIN;
#endif
    curTime = mktime(ct);

    prt = getWeekParity(ct);

    if(argc == 2) { filePath = argv[1]; }
    else { filePath = strcat(getenv("HOME"),"/edu/agenda"); }

    f = fopen(filePath,"r");

    mday = 0;
    endOfLessons = 0;

    while(!feof(f))
    {
        if(fgets(s,100,f))
        {
            if(!strstr(s,"#")) { // Processing lesson

                char* tomorrow;
                char* ar;
                int twd[2];

                iday = getNearestWeekday(ct, atoi(s));
                day = localtime(&iday);
                prt = getWeekParity(day);
                mday = day->tm_mday;

                tomorrow = "";
                ar = "";

                twd[0] = true_wday(ct->tm_wday);
                twd[1] = true_wday(day->tm_wday);
                
                if((twd[0]+1 == twd[1])&&(curTime>endOfLessons))
                {
#ifndef _FANCY
                        tomorrow = "${color #00d000} - завтра";
#else
                        tomorrow = "${color #ff7000}";
#endif
                        ar = "${goto 38}${color #00d000} >> ";
                }

                if((twd[0] == twd[1])&&(curTime<=endOfLessons))
                {
#ifndef _FANCY
                        tomorrow = "${color #00d000} - сейчас";
#else
                        tomorrow = "${color #00d000}";
#endif
                        ar = "${goto 38}${color #00d000} >> ";
                }

#ifdef _CLASSIC
                printf("\n${font}%s${goto 85}${color e9e9e9}%s%s (%s, %d)%s\n",
                        ar, clrD[timeStatus(ct,day,86400)],
                        dOW[atoi(s)-1],
                        monS[day->tm_mon],
                        mday, tomorrow);
#endif
#ifdef _NEW
                printf("\n${font}${goto 35}${color e9e9e9}%s%02d.%02d %s%s${font :6}\n",
                        clrD[timeStatus(ct,day,86400)],
                        day->tm_mon,mday,
                        dOW[atoi(s)-1],
                        tomorrow);
#endif
#ifdef _FANCY
                if(twd[1] == 1) { printf("${font}\n"); }
                printf("${color grey}$hr\n${font}${goto 35}%s%s%s ${alignr}%02d.%02d${color}\n$hr\n",
                        clrD[timeStatus(ct,day,86400)],
                        //"${color grey}",
                        tomorrow,
                        dOW[atoi(s)-1],
                        mday,
                        day->tm_mon+1);
#endif

            } else { // Processing new day

                struct lesson lessonSt;
                struct lesson *les;

                char* time1;
                char* time2;

                char* lesson;
                char* lessonType;

                int type;
                int i;

                les = &lessonSt;
                les->timeSt  = curTimeSt;
                les->time    = &les->timeSt;

                les->timeStr = strtok(s,"#");
                les->srcStr  = strtok(NULL,"#");

                time1=strtok(les->timeStr,":");
                time2=strtok(NULL,":");

                // Date of this lesson
                les->time->tm_year = day->tm_year;
                les->time->tm_mon  = day->tm_mon;
                les->time->tm_mday = day->tm_mday;

                // And its time
                les->time->tm_hour = atoi(time1);
                les->time->tm_min  = atoi(time2);
                les->time->tm_sec  = 0;

                les->duration = 90;
		les->endTime = mktime(les->time) + les->duration * 60;
		
		endOfLessons = les->endTime;

                lesson=les->srcStr;
                lesson=strtok(lesson,"\n");

                if(strstr(lesson,"|"))
                {
                    if(lesson[0]=='|') {
                        if(prt==0) {
                            lesson=NULL;
                        } else {
                            lesson=strtok(lesson,"|");
                        }
                    } else {
                        if ((lesson[strlen(lesson)]=='|')&&(prt==1)) {
                            lesson=NULL;
                        } else {
                            lesson=strtok(lesson,"|");
                            if(prt==1)
                            {
                                lesson=strtok(NULL,"|");
                            }
                        }
                    }
                }

                if(!lesson) { continue; }

                if(strstr(les->srcStr, "|"))
                {
                    lesson=strcat(lesson, "${goto 285}");
                    if(prt==1)
                    {
                        lesson=strcat(lesson, "${color #9999ff}ч/н");
                    } else {
                        lesson=strcat(lesson, "${color #ddaa99}н/н");
                    }

                }

                lessonType=strtok(lesson,":");
                lesson=strtok(NULL,":");

                type=-1;

                if(lessonType)
                {
                    i=0;
                    while(i<5)
                    {
                        if(strcmp(lType[i],lessonType)==0)
                        {
                            type=i;
                            break;
                        }
                        i++;
                    }
                }

                if((lessonType)&&(type>-1))
                {
                    // get auditories
                    char* aud;
                    int tst;
                    double diff;

                    aud = strtok(lesson, "(");
                    aud = strtok(NULL, "(");
                    aud = strtok(aud, ")");
                    les->auditory = aud;

                    diff = timeDiff(ct,les->time);

                    tst = 0;

                    if(diff > 0)
                        { tst = 2; }

                    if((diff >= - les->duration * 60) && (diff < 0))
                        { tst = 1; }


                    printf("${font}${goto 35}%s%02d:%02d${goto 85}%s%s${goto 250}${color grey}%s\n",
                            clrT[tst],
                            les->time->tm_hour,les->time->tm_min,
                            clr[lTC[type]],lesson,les->auditory);

                    if(tst == 1) {
                        printf("${font}${goto 35}${execbar echo %d}\n", - (int)diff / 60 );
                    }

                }

            }
        }
    }

    fclose(f);

    return 0;

}
