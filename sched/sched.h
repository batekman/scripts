/*
=====================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=====================================================================
Author:     batekman@gmail.com
Website:    https://github.com/batekman/scripts
=====================================================================
*/

#ifndef SCHED_H
#define SCHED_H

// Control view-modes via defines
#if !defined(_FANCY) && !defined(_NEW) && !defined(_CLASSIC)
#define _CLASSIC
#endif

struct lesson
{
    int type;
    int duration;
    int endTime;
    char* srcStr;
    char* typeStr;
    char* nameStr;
    char* auditory;
    char* timeStr;
    struct tm timeSt;
    struct tm *time;
};

int true_wday(int wday);
int getNearestWeekday(struct tm *t, int dayOfWeek);
double timeDiff(struct tm *t, struct tm *st);
int timeStatus(struct tm *t, struct tm *st, int dur);
int getWeekParity(struct tm *t);
int main(int argc, char* argv[]);

#endif
