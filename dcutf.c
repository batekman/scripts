/*
 * dcutf.c - program which truncates given string to selected length
 *
 * Copyright (c) 2014-2015 Alexey Shurygin <batekman@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <wchar.h>
#include <unistd.h>

int wrongUsage(char* name) {
    fprintf(stderr, "Usage: %s -n <symbols count> [ -s <ending symbols> | -w ]\n", name);
    exit(EXIT_FAILURE);

    return 1;
}

int main(int argc, char* argv[]) {
    int opt = -1;
    int count = -1;
    int wrap = 0;
    int i = 0;
    int ending_len = 0;

    char *ending = "";

    wchar_t* end;
    wchar_t c;

    setlocale(LC_ALL, "");

    while((opt = getopt(argc, argv, "n:s:w")) != -1) {
        switch(opt) {
            case 'n':
                count = atoi(optarg);
                break;
            case 's':
                ending = optarg;
                break;
            case 'w':
                wrap = 1;
                break;
            default:
                wrongUsage(argv[0]);
        }
    }

    if(count == -1)
        wrongUsage(argv[0]);

    ending_len = mbstowcs(NULL, ending, 0);

    end = calloc(ending_len + 1, sizeof(wchar_t));
    mbstowcs(end,ending,ending_len + 1);

    int skip = 0;

    while(EOF != (c = fgetwc(stdin))) {
        if(skip != 0) {
            if(c == '\n') {
                skip = 0;
            } else {
                continue;
            }
        }
        if((i>=count)&&(c != '\n')) {
            if(wrap == 1) {
                wprintf(L"%s\n", ending);
                i = 0;
            } else {
                wprintf(L"%s", ending);
                i = 0;
                skip = 1;
                continue;
            }
        }
        putwchar(c);
        if(c == '\n')
            i = 0;
        else
            ++i;
    }

    if(i!=0)
        putwchar('\n');

    free(end);

    return 0;
}
