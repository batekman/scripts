#! /usr/bin/env bash

set -e

getSyncTime() {
    if [ ! -f /var/log/emerge.log ]; then
        echo 'N/A'
        return
    fi
    t=`egrep '=== Sync completed'  /var/log/emerge.log | tail -n1 | awk -F':' '{print $1}'`
    if [ -z "$t" ]; then
        echo 'N/A'
    else
        echo "$t"
    fi
}


SColor=31

STIME=`getSyncTime`


if [ x"$STIME" != x'N/A' ]; then

    TDiff=$(( `date "+%s"` - $STIME ))
    Days=$(( 60 * 60 * 24 ))

    if [ $TDiff -gt $(( $Days * 29 )) ]; then
        if [ $TDiff -gt $(( $Days * 365 )) ]; then
            SColor=31
        else
            SColor=33
        fi
    else
        SColor=32
    fi


    STIME=`date --date="@$STIME" "+%Y-%m-%d %H:%M:%S"`
fi


Star='\033[01;'$SColor'm *\033[00m'

echo -e "$Star"' Last sync: '$STIME'\n' >&2


doSync() {
    eix-sync
    return $?
}


doUpgrade() {
    emerge -avutDN --keep-going --backtrack=100 --with-bdeps=y @world
    return $?
}


case $1 in
    i|install)
        emerge -avut $@
    ;;
    r|remove)
        emerge -C $@ || exit 1
        echo -e "\n * You may want to clean obsolete dependencies with \"e clean\"."
    ;;
    d|deselect)
        emerge --deselect $@ || exit 1
        echo -e "\n * You may want to clean obsolete dependencies with \"e clean\"."
    ;;
    clean)
        emerge -ac
        find /var/tmp/portage -mindepth 1 -delete
    ;;
    s|search)
        ARG=`echo "$@" | cut -d " " -f2-`
        eix -nc --format "<category>/<name>\n" --remote --pure-packages $ARG
    ;;
    sl|search-local)
        ARG=`echo "$@" | cut -d " " -f2-`
        eix -nc --format "<category>/<name>\n" --pure-packages $ARG
    ;;
    update)
        doSync
    ;;
    u|upgrade)
        if [ -z "$2" ]; then
            doUpgrade
        else
            emerge -avuN $2
        fi
    ;;
    au|autoupgrade)
        doSync || exit 1
        doUpgrade
    ;;
    d|depends)
        equery depends $2
    ;;
    o|owners)
        portageq owners / $2
    ;;
    f|fix)
        emerge -avt @preserved-rebuild
        revdep-rebuild
        perl-cleaner
        python-updater
        haskell-updater
    ;;
    f|fetch)
        if [ -z "$2" ]; then
            doSync || exit 1
            emerge -fvutDN --keep-going --backtrack=100 --with-bdeps=y @world
        else
            emerge -avutD -f $2
        fi
    ;;
    h|help)
        echo -e "       i[nstall]"
        echo -e "       r[emove]"
        echo -e "       clean"
        echo -e "e      s[earch]        [argument]"
        echo -e "       update"
        echo -e "       u[pgrade]"
        echo -e "       autoupgrade"
        echo -e "       f[etch]"
        echo -e "       h[elp]"
    ;;
    *)
        echo ' * Use "'$0' h[elp]" for info about '$0'.'
    ;;
esac
