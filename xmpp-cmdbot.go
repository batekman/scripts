/*
 * xmpp-cmdbot.go - remote command execution via XMPP protocol (Jabber)
 *
 * Copyright (c) 2015 Alexey Shurygin <batekman@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package main

import (
	"bufio"
	"bytes"
	"crypto/tls"
	"flag"
	"fmt"
	"github.com/mattn/go-xmpp"
	"log"
	"os"
	"os/exec"
	"strings"
	"time"
	"unicode/utf8"
)

var server = flag.String("server", "jabber.ru:5223", "server")
var username = flag.String("username", "", "username")
var password = flag.String("password", "", "password")
var status = flag.String("status", "xa", "status")
var statusMessage = flag.String("status-msg", "", "status message")
var notls = flag.Bool("notls", false, "No TLS")
var debug = flag.Bool("debug", false, "debug output")
var session = flag.Bool("session", true, "use server session")
var timeout = flag.Int("timeout", 300, "idle timeout before shutdown, in seconds")

var workdir = ""

var lastMsgTime = time.Now()

func serverName(host string) string {
	return strings.Split(host, ":")[0]
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "usage: example [options]\n")
		flag.PrintDefaults()
		os.Exit(2)
	}
	flag.Parse()
	if *username == "" || *password == "" {
		flag.Usage()
	}

	if !*notls {
		xmpp.DefaultConfig = tls.Config{
			ServerName:         serverName(*server),
			InsecureSkipVerify: false,
		}
	}

	hostname, _ := os.Hostname()

	resource := "cmdbot at " + hostname

	var talk *xmpp.Client
	var err error
	options := xmpp.Options{Host: *server,
		User:          *username,
		Password:      *password,
		NoTLS:         *notls,
		Debug:         *debug,
		Session:       *session,
		Status:        *status,
		StatusMessage: *statusMessage,
		Resource:      resource,
	}

	talk, err = options.NewClient()

	if err != nil {
		log.Fatal(err)
	}

	go func() {
		if *timeout > 0 {
			for {
				idle := time.Now().Unix() - lastMsgTime.Unix()
				fmt.Printf("%d\n", idle)
				if idle >= int64(*timeout) {
					log.Fatal("Timed out")
				}
				time.Sleep(1 * time.Second)
			}
		}

	}()

	go func() {
		for {
			chat, err := talk.Recv()
			if err != nil {
				log.Fatal(err)
			}
			lastMsgTime = time.Now()
			switch v := chat.(type) {
			case xmpp.Chat:
				log.Print(v.Remote + " " + v.Text)
				remoteJidLength := utf8.RuneCountInString(strings.Split(v.Remote, "/")[0])
				if v.Remote[0:remoteJidLength+1] == *username+"/" {
					go func() {
						var output bytes.Buffer
						if strings.Fields(v.Text)[0] == "-q" {
							msg := "--- Good bye"
							talk.Send(xmpp.Chat{Remote: v.Remote, Type: "chat", Text: msg})
							fmt.Println(msg)
							os.Exit(0)
							return
						}
						cmdstr := strings.Join(strings.Fields(v.Text), " ")

						if strings.Fields(v.Text)[0] == "cd" {
							cmdstr = cmdstr + ";pwd"
						}

						cmd := exec.Command("bash", "-c", cmdstr)
						cmd.Dir = workdir
						cmd.Stdout = &output
						cmd.Stderr = &output

						err := cmd.Run()

						if err != nil {
							fmt.Fprintf(&output, "!! \"%s\" exited with errors\n", v.Text)
							fmt.Fprintf(&output, "%s\n", err)
							log.Print(err)
						} else {
							if strings.Fields(v.Text)[0] == "cd" {
								workdir = strings.TrimRight(output.String(), "\n")
							}
							fmt.Fprintf(&output, "--- \"%s\" completed successfully\n", v.Text)
						}

						talk.Send(xmpp.Chat{Remote: v.Remote, Type: "chat", Text: output.String()})
					}()
				}
				time.Sleep(1 * time.Second)

			}
		}
	}()
	for {
		in := bufio.NewReader(os.Stdin)
		_, _ = in.ReadString('\n')
	}
}
