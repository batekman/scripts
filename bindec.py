#! /usr/bin/env python3

'''
=====================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=====================================================================

bindec.py - small library for binary conversions.

Author:     batekman@gmail.com
Website:    https://github.com/batekman/scripts

=====================================================================

'''

# binary --> decimal
# 101 --> 5

def bin2dec(s):
    total = 0
    numb = s
    for i in range (1, (len(numb))+1):
        if int(numb[-i]) >= 1:
            total = total + (2**(i-1))
    return total

# decimal --> binary
# 5 --> 101

def dec2bin(s, length=0):
    numb = int(s)
    bin = 0
    while numb > 1:
        bin = "%s%s" % (str(numb%2), str(bin))
        numb = numb // 2
    bin = "%s%s" % (str(numb%2), str(bin))
    bin = bin[:-1]
    if length>0:
        while len(bin)<length:
            bin = "0%s" % bin
    return bin

# boolean set --> binary
# True, False --> 10

def bool2bin(s, length=0):
    res=""
    b = False
    for a in s:
        if a:
            res = "%s1" % res
            b = True
        else:
            if b:
                res = "%s0" % res
    if length>0:
        while len(res)<length:
            res = "0%s" % res
    return res

# binary --> boolean list
# 10 --> True, False

def bin2bool(s, length=0):
    res = []
    b = False
    for a in str(s):
        if int(a) >= 1:
            res.append(True)
            b = True
        else:
            if b:
                res.append(False)
    if length>0:
        while len(res) < length:
            res.insert(0, False)
    return tuple(res)

# boolean list --> decimal
# True, False --> 2

def bool2dec(s):
    b = str(bool2bin(s))
    d = bin2dec(b)
    return d

# decimal --> boolean list
# 4 --> True, False, False

def dec2bool(s, length=0):
    b = str(dec2bin(s))
    boo = bin2bool(b, length=length)
    return boo


# Self test

def tests(verbose=False):
    ttt = (
        (bin2dec, ('001010',), 10),
        (dec2bin, (500,), '111110100'),
        (dec2bool, (11,), (True, False, True, True)),
        (bool2dec, ((False, True, False, True, False),), 10),
        (bin2bool, ('011010',), (True, True, False, True, False)),
        (bool2bin, ((False, True, False, False, True),), '1001'),
        (dec2bin, (12, 7), '0001100'),
        (dec2bool, (11, 5), (False, True, False, True, True)),
        (bin2bool, ('01102', 5), (False, True, True, False, True)),
        (bin2dec, ('010203',), 21)
        )

    for func, data, result in ttt:
        r = func(*data)
        if r != result:
            print('!! %s(%s) --> %s' % (func.__name__, data, r))
        if r == result and verbose:
            print('== %s(%s) --> %s' % (func.__name__, data, r))
