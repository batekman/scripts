#! /usr/bin/env bash

# tasf.sh
#
# Copyright (c) 2014-2015 Alexey Shurygin <batekman@gmail.com>
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


set -e

TASKDIR="$HOME/tasks"
ARCHIVEDIR="$HOME/.tasks-archive"

Reset=$'\e[0m'

Purple=$'\e[0;35m'
BPurple=$'\e[1;35m'
Green=$'\e[0;32m'
Blue=$'\e[0;34m'
BBlue=$'\e[1;34m'
DarkBlue=$'\e[0;36m'
Red=$'\e[0;31m'
BRed=$'\e[1;31m'

TermWidth=`stty size | awk '{print $2}'`


dots() {
    count=$1
    if [ -z "$count" ]; then exit 1; fi
    for i in $(seq ${count}) ; do echo -n '.'; done
}

touchDir() {
    if [ ! -e "$1" ]; then
        mkdir -p "$1"
    elif [ ! -d "$1" ]; then
        echo "$BRed"'Path '"$BBlue\"$1\"$BRed"' is exists and is not a directory.' >&2
        echo "$BRed"'Please delete or move this object.'"$Reset" >&2
        exit 1
    fi
}

processFileExistence() {
    fname="$1"

    if [ ! -f "$fname" ]; then
        echo $BRed'File'" $BBlue\"$fname\"$BRed "'not found'$Reset >&2
        exit 1
    fi
}

nameWithMtime() {

    local pretty lines length mt datelen c

    pretty=`basename "$1"`
    pretty="${pretty/.txt/}"

    lines=`wc -l "$1" | awk '{print $1}'`
    length=$(( ${#pretty} + 5 + ${#lines} ))
    mt=`date -r "$1" '+%-d %b %Y %H:%M:%S'`
    datelen="${#mt}"

    echo -n "$BPurple$pretty$Purple ($lines)"

    if [ -z "$2" ]; then
        c=$(( $TermWidth - $datelen - $length ))
        dots $c
        echo -n "$Reset${mt}$Purple..${Reset}"
    else
        echo -n " $Reset${mt} "'${hr}'
    fi
}

list() {
    touchDir "$TASKDIR"
    ls -rt "$TASKDIR/" | while read file; do
        file="$TASKDIR/$file"
        if [ -f "$file" ] && [ -s "$file" ]; then
            echo "$file"
        fi
    done
}

listAll() {
    touchDir "$TASKDIR"
    ls -rt "$TASKDIR/" | while read file; do
        file="$TASKDIR/$file"
        if [ -f "$file" ]; then
            echo "$file"
        fi
    done
}

archive() {
    touchDir "$TASKDIR"
    touchDir "$ARCHIVEDIR"
    cd "$TASKDIR/.."
    local name=`basename "$TASKDIR"`
    tar -cpjf "$ARCHIVEDIR/tasks-`date '+%Y-%m-%d-%H-%M-%S'`.tar.bz2" "$name"
}

show() {

    local file sline name

    file=$1

    if [ ! -z $i ]; then
        echo
    fi
    i=1

    sline=`head -n1 "$file"`

    if [[ ! -z "$2" && "$2" == "noFormat" ]]; then
        echo "$file"

        cat "$file"
    else
        echo `nameWithMtime "$file" "$2"`

        rule1="s/\(.*\) - \(.*\)/${Green}\1${Reset} - \2/"
        rule2="s/\(.*\):$/${DarkBlue}\1:${Reset}/"
        sed "${rule1};${rule2}" "${file}"
    fi


}

showForConky() {
    Purple=$'${color purple}'
    BPurple="$Purple"
    Green=$'${color green}'
    BGreen="$Green"
    Blue=$'${color blue}'
    BBlue="$Blue"
    Reset=$'${color}'
    DarkBlue=$'${color #005599}'
    show "$1"
}

if [ ! -z "$1" ]; then
    case "$1" in
        "-e"|"--edit")
            fname="$TASKDIR/${2/.txt/}.txt"
            $EDITOR "$fname"
            if [ -f "$fname" ]; then
                show "$fname"
            fi
            exit 0
        ;;
        "-d"|"--delete")
            fname="$TASKDIR/${2/.txt/}.txt"

            processFileExistence "$fname"

            archive

            if [ -f "$fname" ]; then
                rm -v "$fname"
            fi
            exit 0
        ;;
        "-c"|"--clean")
            fname="$TASKDIR/${2/.txt/}.txt"
            processFileExistence "$fname"

            archive

            if [ -f "$fname" ]; then
                cat /dev/null > "$fname"
            fi
            echo 'File "'${2/.txt/}'" cleaned.'
            exit 0
        ;;
        "--flush")
            archive
            list | while read file; do
                cat /dev/null > "$file"
            done
            exit 0
        ;;
        "-t"|"--titles")
            listAll | while read file; do
                nameWithMtime "$file"
            done
            echo
            exit 0
        ;;
        "--archive")
            archive
            exit 0
        ;;
        '--conky')
            list | while read file; do
                showForConky "$file"
            done
            exit 0
        ;;
        '-h'|'--help')
            Pname=`basename "$0"`
            echo "$Pname -- true-minimalistic task manager"
            echo "Usage:"
            echo "    $Pname                      show all files"
            echo "    $Pname -t|--titles          show only names"
            echo "    $Pname -e|--edit <name>     create or edit file with \$EDITOR ($EDITOR)"
            echo "    $Pname -d|--delete <name>   delete file"
            echo "    $Pname -c|--clean <name>    clean file"
            echo "    $Pname --conky              show all files in conky format"
            echo "    $Pname --flush              clear all files"
            echo "    $Pname -f|--fast            fast mode without formatting"
            echo "    $Pname --archive            create backup from current working directory"

            exit 0
        ;;
        '-f')
            list | while read file; do
                show "$file" noFormat
            done
            exit 0
        ;;
        *)
            processFileExistence "$TASKDIR/$1.txt"
            show "$TASKDIR/$1.txt"
            exit 0
        ;;

    esac
fi

list | while read file; do
    show "$file"
done
