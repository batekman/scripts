#! /usr/bin/env bash

[ x"$1" = x"--conky" ] && mConky=1

[ x"$mConky"=x"1" ] && {

	cWhite='${color white}'
	cGrey='${color grey}'
	cDarkGrey='${color 707070}'
	cBlue='${color #9999FF}'
	cPink='${color #ddaa99}'
	cOrange='${color orange}'
	cGreen='${color green}'
	cCyan='${color cyan}'
	cGreenGrey='${color #90a090}'

	cRst='${color white}'

}

agFile="${HOME}/edu/agenda"

agClr[0]="$cOrange"
agClr[1]="$cGreen"
agClr[2]="$cBlue"
agClr[3]="$cGreenGray"
agClr[4]="$cCyan"

agLbl[0]="практика"
agLbl[1]="лекция"
agLbl[2]="лабораторная"
agLbl[3]="по выбору"
agLbl[4]=""

parClr[0]="$cBlue"
parClr[1]="$cPink"

parLbl[0]="чётная"
parLbl[1]="нечётная"

mParityInvert=0

curDayNumber=`date "+%u"`
curDayLbl=`date "+%A:"`

echo "$curDayNumber" | egrep -q '^[6|7]$' && weekend=1
weekNumber=`date "+%V"`
curTime=$(( `date "+%H"` * 60 + `date "+%M"` ))

[ x"$weekend" = x"1" ] && {
	case $curDayNumber in
		6)
			showDayNumber=`date --date='+2 day' '+%u'`
			showDayLbl=`date --date='+2 day' '+%A:'`
		;;
		7)
			showDayNumber=`date --date='+1 day' '+%u'`
			showDayLbl=`date --date='+1 day' '+%A:'`
		;;
	esac
} || {
	showDayNumber="${curDayNumber}"
	showDayLbl="${curDayLbl}"
}

parity() {
	[ $(( $(echo $weekNumber | sed 's/0//') / 2 )) -eq $(( $(echo $weekNumber | sed 's/0//') * 2 / 2 )) ] && {
		res="$mParityInvert" 
	} || {
		[ x"$mParityInvert" = x"1" ] && {
            res=1
        } || {
			res=0
	    }
	}

	[ x"$weekend" = x"1" ] && [ x"$res" = x"1" ] && res=0
	[ x"$weekend" = x"1" ] && [ x"$res" = x"0" ] && res=1

	echo $res
}

show_parity() {

	local parity=`parity`
	local res="${parClr[$parity]}${parLbl[$parity]}$cRst"

	[ ! -z $weekend ] && res="следующая неделя: ${res}" || res="текущая неделя: ${res}"
	echo "${res}"
}

parse_line() {

	local res="${2}"

	case x"${1}" in
		x"time")
			echo "${res}" | grep '#' -q && {
				res=`echo "${res}" | awk -F'#' '{print $1}'`
			}
		;;
		x"name")
			res=`echo "${res}" | awk -F'#' '{print $2}'`
			echo "${res}" | grep '|' -q && {
				[ `parity` -eq 0 ] && res=`echo "${res}" | awk -F'|' '{print $2}'`" ${cDarkGrey}(ч/н)"
				[ `parity` -eq 1 ] && res=`echo "${res}" | awk -F'|' '{print $1}'`" ${cDarkGrey}(н/н)"
			}
		;;
		*) res= ;;
	esac

	echo "${res}"
}

parse_type() {

		local res=`parse_line name "${1}"`

		[ -z "${res}" ] || {
			res=`echo '$font'"$cGrey ${2}"'${tab}${tab}'"${res}"`
			res=`echo "$res" | sed 's/Лаб:\(.*\)$/\${color #9999FF}\1\${color grey} \${alignr} лабораторная\$color/'`
			res=`echo "$res" | sed 's/Л:\(.*\)$/\${color green}\1\${color grey} \${alignr} лекция\$color/'`
			res=`echo "$res" | sed 's/Пр:\(.*\)$/\${color orange}\1\${color grey} \${alignr} практика\$color/'`
			res=`echo "$res" | sed 's/Вб:\(.*\)$/\${color #90a090}\1\${color grey} \${alignr} по выбору\$color/'`
			res=`echo "$res" | sed 's/Производственная практика/\${color cyan}Производственная практика\${color grey}\$color/'`
			echo "${res}"
		}
}

show_dayLabel() {
	[ x"$showDayNumber" = x"$curDayNumber" ] && clr="$cWhite" || clr="$cGrey"
	echo -ne "\n${font}$clr"$showDayLbl"$cRst\${font :size=6}\n"
	echo

}

get_minutes() {

	local hours=`echo ${1} | awk -F':' '{print $1}'`
	local hours=${hours#0}
	local minutes=`echo ${1} | awk -F':' '{print $2}'`
	local minutes=${minutes#0}

	echo $(( $hours * 60 + $minutes ))

}

check_current() {

	local res=
	local startTime=`get_minutes ${1}`
	local endTime=$(( $startTime + 95 ))
	local total=$(( $endTime - $startTime ))
	local elapsed=$(( $endTime - $curTime ))
	local last=$(( $total - $elapsed ))

	[ $curTime -ge $startTime ] && {
		[ $(( $curTime )) -gt $(( $startTime + 95 )) ] && res=5 # пара уже кончилась
		[ $curTime -lt $(( $startTime + 45 )) ] && res=1 # идёт первое занятие пары
		[ $curTime -ge $(( $startTime + 45 )) ] && [ $curTime -lt $(( $startTime + 50 )) ] && res=2 # перемена 5 минут
		[ $curTime -ge $(( $startTime + 95 )) ] && [ $curTime -lt $(( $startTime + 105 )) ] && res=3 # перемена 10 минут
		[ $curTime -ge 790 ] && [ $curTime -lt 820 ] && res=6 # перемена 20 минут
		case x"$2" in
			x'percentage') res=`echo "scale=0; $last*100/95" | bc`'%' ;;
			x'elapsed') res="$elapsed" ;;
		esac
	} || {
		res=7
		[ $curTime -lt $(( $startTime )) ] && res=0 # пара ещё не начиналась
	}
	[ ! -z $weekend ] && res=0

	echo "${res}"

}

show_ag() {

	local s=0
	local i=1
	local state=

	cat "${agFile}" | while read line; do {
		echo $line | egrep '^[0-9]$' -q && {
			[ $(( $line )) -lt $(( $showDayNumber )) ] && s=1
			[ $(( $line )) -eq $(( $showDayNumber )) ] && {
				s=0
				i=1
				show_dayLabel
			}
			[ $(( $line )) -gt $(( $showDayNumber )) ] && break
			echo $(( $line )) | egrep '[6|7]' -q && break
			continue
		}

		[ x"$s" = x"1" ] && continue

		lTime=`parse_line time "${line}"`
		mTime=`get_minutes "${lTime}"`
		state=$(check_current "$lTime")
		name=`parse_type "${line}"`

		case "$state" in
			5) clr="${cGrey}" ;;
			0) clr="${cWhite}" ;;
			1) clr='${color #00ff00}' ;;
		esac

		[ ! -z "$name" ] && echo '${goto 35}${font}'"${clr}${lTime}"'${goto 65}'"${cWhite}${name}"
		
		[ "$state" = "1" ] && echo '${goto 35}${execbar sched bar '"${lTime}"'}${alignr}${color grey}'"осталось `check_current "$lTime" elapsed` минут"
		i=$(( i+1 ))
	} done
}

[ x"$1" = x"bar" ] && {
	check_current "$2" percentage
	exit 0
}

show_parity
show_ag
