#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define WAKECONTROL "/sys/class/rtc/rtc0/wakealarm"

int invalidUsage(char* myname)
{
    fprintf(stderr, "Usage: %s < HH:MM | off | 0 >\n", myname);
    return 0;
}

time_t stringToGMT(const char* src)
{
    char s[6];

    char* hour;
    char* min;

    time_t current, res, tmp;

    struct tm *t;

    // Error if string not contains colon
    if(!strstr(src, ":")) { return 0; }

    strncpy(s, src, 6);

    hour = strtok(s, ":");
    min = strtok(NULL, ":");

    current = time(NULL);
    t = localtime(&current);

    if(
        (atoi(hour) < t->tm_hour) ||
        (
            (atoi(hour) == t->tm_hour) &&
            (atoi(min) <= t->tm_min)
        )
      )
    {
        tmp = mktime(t);
        tmp = tmp + 86400;
        t = localtime(&tmp);
    }

    t -> tm_hour = atoi(hour);
    t -> tm_min  = atoi(min);
    t -> tm_sec  = 0;

    res = timegm(t);

    return res;
}

int showStatus()
{
    FILE *f;
    char c;

    f = fopen("/proc/driver/rtc", "r");
    if(f) { fprintf(stderr, "stdout << /proc/driver/rtc\n\n"); }
    while((c = getc(f)) != EOF)
    {
        putchar(c);
    }
    fclose(f);

    return 0;
}

int setWakeup(time_t wakeTime)
{
    FILE *f;

    fprintf(stderr, "%d >> %s\n\n", (int)wakeTime, WAKECONTROL);

    f = fopen(WAKECONTROL, "w");
    
    if(!f) {
        fprintf(stderr, "Can't open %s\n", WAKECONTROL);
        return 1;
    }

    fprintf(f, "0\n");
    fclose(f);

    f = fopen(WAKECONTROL, "w");
    fprintf(f, "%d\n", (int)wakeTime);
    fclose(f);

    showStatus();
    
    return 0;
}

int main(int argc, char* argv[])
{
    char src[6];

    time_t t;

    int res;

    if(argc != 2)
    {
        invalidUsage(argv[0]);
        return 2;
    }

    if
        (
            (strcmp(argv[1], "off") == 0) ||
            (strcmp(argv[1], "0") == 0)
        )
    {
        setWakeup(0);
        return 0;
    }

    strncpy(src, argv[1], 6);

    t = stringToGMT(src);
    if(!t)
    {
        invalidUsage(argv[0]);
        return 2;
    }

    res = setWakeup(t);

    return res;
}
