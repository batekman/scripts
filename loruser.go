package main

import (
	"database/sql"
	"errors"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"os"
	"regexp"
	"strconv"
)

func check(e error) {
	if e != nil {
		log.Fatal(e)
	}
}

func initDB() *sql.DB {
	db, err := sql.Open("sqlite3", "lorusers.db")
	check(err)

	query := `
	CREATE TABLE IF NOT EXISTS
	users (
		id INTEGER PRIMARY KEY,
		name TEXT UNIQUE NOT NULL
	);
	`
	_, err = db.Exec(query)
	check(err)
	return db
}

func getID(name string) (id int, err error) {
	if name == "" {
		err = errors.New("Given name string is empty")
		return -1, err
	}
	db := initDB()
	defer db.Close()
	stmt, err := db.Prepare("SELECT id FROM users WHERE name=?")
	check(err)
	defer stmt.Close()
	err = stmt.QueryRow(name).Scan(&id)
	check(err)

	return id, nil
}

func getName(id int) (name string, err error) {
	if id < 0 {
		err = errors.New("Chosen ID is less than 0")
		return "", err
	}
	db := initDB()
	defer db.Close()
	stmt, err := db.Prepare("SELECT name FROM users WHERE id=?")
	check(err)
	defer stmt.Close()
	err = stmt.QueryRow(id).Scan(&name)
	check(err)
	return name, nil
}

func listUsers() {
	db := initDB()
	defer db.Close()
	rows, err := db.Query("SELECT id, name FROM USERS")
	check(err)
	defer rows.Close()
	for rows.Next() {
		var id int
		var name string
		rows.Scan(&id, &name)
		fmt.Printf("%d    : %s\n", id, name)
	}
	rows.Close()
}

func addUser(id int, name string) {
	uniqErrorRe, err := regexp.Compile(`^UNIQUE constraint failed\: users\.([a-zA-Z-_]+)$`)
	check(err)
	db := initDB()
	defer db.Close()
	cur, err := db.Begin()
	check(err)
	stmt, err := cur.Prepare("INSERT INTO users VALUES (?, ?)")
	check(err)
	defer stmt.Close()
	_, err = stmt.Exec(id, name)
	if err != nil {
		errstr := fmt.Sprint(err)
		var outstr string
		a := uniqErrorRe.MatchString(errstr)
		if a {
			outstr = uniqErrorRe.FindStringSubmatch(errstr)[1]
			log.Fatal("User with this " + outstr + " is already exists.")
		} else {
			check(err)
		}
	}
	cur.Commit()
	fmt.Printf("%s added with id %d\n", name, id)
}

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Too few arguments")
	}

	cmd := os.Args[1]

	var args []string

	if len(os.Args) >= 3 {
		args = os.Args[2:]
	}

	switch {
	case cmd == "list":
		listUsers()
	case cmd == "add":
		id, err := strconv.Atoi(args[0])
		check(err)
		addUser(id, args[1])
	case cmd == "name":
		id, err := strconv.Atoi(args[0])
		check(err)
		name, err := getName(id)
		check(err)
		fmt.Printf("%s\n", name)
	case cmd == "id":
		name := args[0]
		id, err := getID(name)
		check(err)
		fmt.Printf("%d\n", id)
	case cmd == "list":
		listUsers()
	}

}
