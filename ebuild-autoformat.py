#! /usr/bin/env python3

'''
=====================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=====================================================================
'''

import sys, os, re, time, shutil

HEADER = '/usr/portage/header.txt'

def main():
    if len(sys.argv) != 2:
        sys.stderr.write('Invalid arguments count (must be one).\n')
        return 2

    fname = sys.argv[1]

    if not os.path.isfile(fname):
        sys.stderr.write('"%s" is not a file!\n' % fname)
        return 1

    all_lines = []
    headless_lines = []
    new_lines = []

    f = open(fname, 'r')

    for line in f:
        all_lines.append(line)

    f.close()

    # Reading header

    header_lines = []

    h = open(HEADER, 'r')

    for line in h:
        new_lines.append(line)

    h.close()


    write_lines = False

    for line in all_lines:
        if not re.match('^#', line) and not re.match('^\n$', line):
            write_lines = True
        if write_lines:
            new_lines.append(line)

    # Don't overwrite if file is already correct

    if set(all_lines) & set(new_lines) == set(all_lines):
        return 0

    shutil.move(fname, '%s.af-bak~' % fname)

    f = open(fname, 'w')

    for line in new_lines:
        f.write(line)

    f.close()

if __name__ == '__main__':
    sys.exit(main())
